<?php

use Carbon\Carbon;

/**
 * Send email
 *
 * @return response()
 */
if (! function_exists('sendEmail')) {
    function sendEmail($data)
    {
        $user = $data['user'];
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("testingteam220@gmail.com", "Love2tip");
        $email->setSubject("WELCOME TO Love2tip!");
        $email->addTo($user->email, $user->first_name);
        $email->addContent("text/html", $data['html']);
        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
        } catch (Exception $e) {
            return response(['status_code' => 400, 'message' => 'Error', 'data' => $e->getMessage()]);
        }
        return $response;
    }
}
