<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Exception;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'         => 'required',
            'email'        => 'required|email|unique:users',
            'password'     => 'required',
            'device_type'  => 'required',
            'device_token' => 'required',
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'message' => $validator->errors()
            ];
            return response()->json($response, 400);
        }

        try {
            $create = User::create([
                'name'         => $request->name,
                'email'        => $request->email,
                'password'     => Hash::make($request->password),
                'device_type'  => $request->device_type,
                'device_token' => $request->device_token,
            ]);
            $access_token = $create->createToken('authToken')->accessToken;
            $otp = rand(1000, 9999);
            $update = User::where('email', '=', $request->email)->update([
                'otp'            => $otp,
                'remember_token' => $access_token
            ]);

            $user = User::find($create->id);

            /* Send Grid Mail */
            $html =  view('templates.emails.welcome', compact('otp'))->render();
            $email_data = [
                'user' => $user,
                'html' => $html
            ];
            sendEmail($email_data);

            $response = [
                'success' => true,
                'data'    => $user,
                'message' => 'User register successfully!'
            ];

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'data'    => $e->getMessage()
            ];

            return response()->json($response, 400);
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'         => 'required',
            'password'      => 'required',
            'device_token'  => 'required',
            'device_type'   => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'message' => $validator->errors()
            ];
            return response()->json($response, 400);
        }
        try {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $data          = User::find(Auth::id());
                $accessToken   = $data->createToken('authToken')->accessToken;
                $update        = User::where('id', $data->id)->update(['remember_token' => $accessToken]);
                $user          = User::firstWhere('id', $data->id);
                $user->token   = $accessToken;

                $response = [
                    'success' => true,
                    'data'    => $user,
                    'message' => 'User login successfully!'
                ];
                return response()->json($response, 200);
            } else {
                $response = [
                    'success' => false,
                    'message' => 'Unauthorised'
                ];
                return response()->json($response, 200);
            }
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage()
            ];
            return response()->json($response, 400);
        }
    }

    public function socialLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'          => 'required',
            'social_id'     => 'required',
            'device_token'  => 'required',
            'device_type'   => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'message' => $validator->errors()
            ];
            return response()->json($response, 400);
        }

        try {
            $check_user = User::where('social_id', $request->social_id)->first();
            if ($check_user) {
                User::where('id', $check_user->id)->update([
                    'device_token'  => $request->device_token,
                    'device_type'   => $request->device_type,
                    'social_id'     => $request->social_id
                ]);

                $accessToken   = $check_user->createToken('authToken')->accessToken;
                $update        = User::where('id', $check_user->id)->update(['remember_token' => $accessToken]);
                $user          = User::where('id', $check_user->id)->first();
                $user->token   = $accessToken;
                $response = [
                    'success' => true,
                    'data'    => $user,
                    'message' => 'User login successfully!'
                ];
                return response()->json($response, 200);
            } else {
                $user = new User([
                    'social_id'     =>  $request->social_id,
                    'name'          =>  $request->name,
                    'device_type'   =>  $request->device_type,
                    'device_token'  =>  $request->device_token
                ]);
                $user->save();

                $accessToken   = $user->createToken('authToken')->accessToken;
                $update        = User::where('id', $user->id)->update(['remember_token' => $accessToken]);
                $user          = User::firstWhere('id', $user->id);
                $user->token   = $accessToken;
                $response = [
                    'success' => true,
                    'data'    => $user,
                    'message' => 'User register successfully!'
                ];
                return response()->json($response, 200);
            }
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage()
            ];
            return response()->json($response, 400);
        }

    }

    public function verifyOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'otp'   => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'message' => $validator->errors()
            ];
            return response()->json($response, 400);
        }
        try {
            $check_user  = User::where([['email', $request->email], ['otp', $request->otp]])->first();
            if ($check_user) {
                $accessToken = $check_user->createToken('authToken')->accessToken;
                $update =  User::where('email', $request->email)->update(['is_verified' => 1, 'remember_token' => $accessToken]);
                $user = User::find($check_user->id);
                $user->token   = $accessToken;
                $response = [
                    'success' => true,
                    'data'    => $user,
                    'message' => 'OTP verified successfully!'
                ];
                return response()->json($response, 200);
            } else {
                $response = [
                    'success' => false,
                    'message' => 'OTP does not match!'
                ];
                return response()->json($response, 400);
            }
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage()
            ];
            return response()->json($response, 400);
        }
    }

    public function resendOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'message' => $validator->errors()
            ];
            return response()->json($response, 400);
        }

        try {
            $otp = mt_rand(1000, 9999);
            $user = User::where('email', '=', $request->email)->update(['otp' => $otp]);

            if ($user) {
                $user = User::firstWhere('email', '=', $request->email);
                /* Send Grid Mail */
                $html =  view('templates.emails.resend_otp', compact('otp'))->render();
                $email_data = [
                    'user' => $user,
                    'html' => $html
                ];
                sendEmail($email_data);
                $response = [
                    'success' => true,
                    'data'    => $user,
                    'message' => 'OTP verified successfully!'
                ];
                return response()->json($response, 200);
            } else {
                $response = [
                    'success' => false,
                    'message' => 'Email does not exist!'
                ];
                return response()->json($response, 400);
            }
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage()
            ];
            return response()->json($response, 400);
        }
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'message' => $validator->errors()
            ];
            return response()->json($response, 400);
        }

        try {
            $otp = mt_rand(1000, 9999);
            $user = User::where('email', '=', $request->email)->update(['otp' => $otp]);

            if ($user) {
                $user = User::firstWhere('email', '=', $request->email);
                /* Send Grid Mail */
                $html =  view('templates.emails.forgot_password', compact('otp'))->render();
                $email_data = [
                    'user' => $user,
                    'html' => $html
                ];
                sendEmail($email_data);
                $response = [
                    'success' => true,
                    'data'    => $user,
                    'message' => 'OTP verified successfully!'
                ];
                return response()->json($response, 200);
            } else {
                $response = [
                    'success' => false,
                    'message' => 'Email does not exist!'
                ];
                return response()->json($response, 400);
            }
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage()
            ];
            return response()->json($response, 400);
        }
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'            => 'required',
            'password'         => 'required|min:6',
            'confirm_password' => 'required|same:password|min:6'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'message' => $validator->errors()
            ];
            return response()->json($response, 400);
        }

        try {
            $password           = $request->get('password');
            $user               = User::where('email', $request->email)->first();
            $user->password     = bcrypt($password);
            $user->save();
            $response = [
                'success' => true,
                'data' => $user,
                'message' => 'OTP verified successfully!'
            ];
            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage()
            ];
            return response()->json($response, 400);
        }
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'message' => $validator->errors()
            ];
            return response()->json($response, 400);
        }

        try {
            if (Hash::check($request->old_password, Auth::user()->password)) {
                User::where('id', Auth::id())->update([
                    'password' => Hash::make($request->new_password)
                ]);
                $response = [
                    'success' => true,
                    'message' => 'Password changed successfully!'
                ];
                return response()->json($response, 200);
            } else {
                $response = [
                    'success' => false,
                    'message' => 'Old password does not match!'
                ];
                return response()->json($response, 400);
            }
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage()
            ];
            return response()->json($response, 400);
        }
    }

    public function getProfile(Request $request)
    {
        $user = Auth::user();
        $response = [
            'success' => true,
            'data' => $user
        ];
        return response()->json($response, 200);
    }
}
