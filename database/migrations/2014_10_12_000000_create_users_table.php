<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->integer('social_id')->nullable();
            $table->integer('device_type')->nullable()->comment('0 for android , 1 for ios, 2 for web');
            $table->longText('device_token')->nullable();
            $table->string('password');
            $table->integer('otp')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('is_verified')->default(0);
            $table->longText('remember_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
