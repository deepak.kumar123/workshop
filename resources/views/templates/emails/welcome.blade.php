
<table width="100%" height="100%" border="0"  align="center" cellpadding="0" cellspacing="0" bgcolor="#fff">

    <!-- START HEADER/BANNER -->
    <tbody>
        <tr>

            <td align="center">
                <table width="600" border="0" align="center" cellpadding="50"  cellspacing="0">
                    <tbody>
                        <tr>
                            <td align="center" valign="top"  bgcolor="#ffffff" style="border-radius: 15px; position: relative;">
                                <table class="col-800" width="800" height="200" border="0" border-radius: "15px";  align="center" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="font-size:37px; font-weight: bold;  padding: 30px 0px; position: relative;">
                                                <img height="60" src="https://www.codobux.com/images/logo-dark-codobux.svg"  alt="Codobux">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="50"></td>
                                        </tr>

                                        <tr>
                                            <td align="center" style="font-family: 'Raleway', sans-serif; font-size:30px; color:#171717; font-weight: 700;">
                                                <p style="font-family: 'Raleway', sans-serif; font-size:17px; color:#171717; font-weight: 700; "> Welcome to Cleopatras! </p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center" style="font-family: 'Raleway', sans-serif; font-size:17px; color:#171717; font-weight: 700;">
                                                <p style="font-family: 'Raleway', sans-serif; font-size:17px; color:#171717; font-weight: 700;"> You're receiving this message because you signed up for an account on Cleopatras Lounge. (IF you didn't sign up, you can ignore this email.)</p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left" style="font-family: 'Raleway', sans-serif; font-size:16px; color:#000000; font-weight: 700;">
                                                <p style="font-family: 'Raleway', sans-serif; font-size:17px; color:#171717; font-weight: 700; ">
                                                    Please use the following one-time password to verify yourself - <span style="font-weight: 800; font-family: poppins;">{{ $otp }}</span>
                                                </p>
                                            </td>

                                        <tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
