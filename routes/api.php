<?php

use App\Http\Controllers\Api\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('verify_otp', 'verifyOtp');
    Route::post('resend_otp', 'resendOtp');
    Route::post('forgot_password', 'forgotPassword');
    Route::post('reset_password', 'resetPassword');
    Route::post('social_login', 'socialLogin');

});

Route::middleware(['auth:api'])->group(function () {
    Route::controller(AuthController::class)->group(function () {
        Route::post('change_password', 'changePassword');
        Route::get('get_profile', 'getProfile');
    });

});


